BD=test
BDU=test
PWD=test

service mysql start && mysql -e "CREATE DATABASE IF NOT EXISTS $BD" && mysql -e "GRANT ALL PRIVILEGES ON $BDU.* TO $BD@localhost IDENTIFIED BY '$PWD'" && service apache2 start && tail -f /var/log/apache2/access.log

# import an existing bd backup
#mysql -u $BDU -p$PWD $BD < /bd.sql
