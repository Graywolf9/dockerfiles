FROM debian:11.5-slim

# Add repo for php7.0
RUN apt update
RUN apt install -y ca-certificates apt-transport-https wget gpg
RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
RUN echo 'deb https://packages.sury.org/php/ bullseye main' | tee /etc/apt/sources.list.d/php.list

# Install apache2, php7.0 and libs
RUN apt update
RUN apt install -y sudo apache2 php7.0 

# Installation of php7.0 modules
#RUN apt install -y php7.0-mbstring php7.0-sqlite3 php7.0-zip php7.0-xml php7.0-curl php7.0-mysqli php7.0-gd php7.0-intl php7.0-xmlrpc php7.0-soap

# Enable apache2 rewrite module
RUN a2enmod rewrite

CMD service apache2 start && tail -f /var/log/apache2/access.log
